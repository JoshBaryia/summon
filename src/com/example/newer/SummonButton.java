package com.example.newer;

import android.content.Context;
import android.graphics.Color;
import android.widget.Button;

public class SummonButton extends Button{
	
	public int id;
	
	public SummonButton(Context context){
		
		super(context);
		
	}

	public SummonButton(Context context, int summonId) {
		super(context);
		this.id = summonId;
	
		this.setTextColor(Color.parseColor("#000077"));
		this.setBackgroundColor(Color.parseColor("#eeeeee"));
		
	}
	
	public int getId(){
		
		return id;
	}

	public void setId(int newId){
		
		this.id = newId;
	}
	
	
}

package com.example.newer;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class AddFriend extends Activity {
	
	
	EditText	friendNameText;
	TextView	topText;
	
	Button		addFriendButton,
				requestButton;
	
	private static final String PREFS_NAME = "UserDetails";
	private SharedPreferences settings;
	String username;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_friend);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		settings 		= 	getSharedPreferences(PREFS_NAME, 0);
		username 		= 	settings.getString("email", "not signed in!");
		
		topText 		= (TextView)findViewById(R.id.add_friends_topText);
		
		topText.setText(settings.getString("email", "not signed in!"));
		
		friendNameText 	= (EditText)findViewById(R.id.add_friends_friendText);
		
		addFriendButton = (Button)findViewById(R.id.add_friends_addButton);
		requestButton  	= (Button)findViewById(R.id.add_friend_requests_button);

	}

	public void addFriend(View v){
		
		Toast.makeText(this, "Send clicked", Toast.LENGTH_SHORT).show();
		Thread t = new Thread(new Runnable(){
			@Override
			public void run(){
				send();
			}
		});
		t.run();
		//send();
	}
	
	public void send(){
		 
		if(settings.getString("email", "").equals("")) {
			
			Toast.makeText(this, "not logged in" , Toast.LENGTH_SHORT).show();
			return;
			
		}
		
		if(friendNameText.getText().toString().equals("")) {
			
			Toast.makeText(this, "Enter a friend's name!", Toast.LENGTH_SHORT).show();
			return;
			
		}
		
		String url = "http://109.255.215.99/summon/addFriend.php";
		
		try{
			
			HttpClient 	httpclient	= new DefaultHttpClient();
			
			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("email1",settings.getString("email", "not signed in!")));
			nameValuePairs.add(new BasicNameValuePair("email2",friendNameText.getText().toString()));
			HttpPost 	httppost 	= new HttpPost(url);
			
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			
			httpclient.execute(httppost);
			
			Toast.makeText(this,"Request Sent! uname: " + username  + "frname= " + friendNameText.getText().toString(),Toast.LENGTH_SHORT).show();
			
			}catch(Exception e){
			
				Toast.makeText(this, "Exception :/", Toast.LENGTH_SHORT).show();
			
			}
		
	}
		
	
	
	public void requestsClicked(View v){
		
		Intent i = new Intent(this, Requests.class);
		startActivity(i);
		
	}
	
}

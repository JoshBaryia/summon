package com.example.newer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Friends extends Activity {
	
	private			LinearLayout 		friendSpace;

	private static 	final String 		PREFS_NAME = "UserDetails";
	private 		SharedPreferences 	settings;
	
	private			TextView	 		info;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_friends);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		friendSpace 	= (LinearLayout)findViewById(R.id.friend_friend_space);
		info  			= (TextView)findViewById(R.id.friends_info);
		
		displayFriends(
				getFriends()
				);
		
	}
	
	public void addFriendsClicked(View v){
		
		Intent i = new Intent(this, AddFriend.class);
		startActivity(i);
		
	}

	private String[] getFriends(){
		
		String[] 	def 		= {};
		String 		url  		= "http://109.255.215.99/summon/getFriends.php"; //?email=" + me;
		String 		listString 	= "";
		
		settings 				= getSharedPreferences(PREFS_NAME, 0);
		String 		me 			= settings.getString("email", "");
		
		if(me.equals("")) 		return def;
		
		 try{
				HttpClient 		httpclient 	= 	new DefaultHttpClient();
				HttpPost 		httppost 	=	new HttpPost(url);
				
				ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
		        nameValuePairs.add(new BasicNameValuePair("email", me));
		        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				
				HttpResponse 	response 	= httpclient.execute(httppost);
				HttpEntity 		entity		= response.getEntity();
				InputStream 	i 			= entity.getContent();
				BufferedReader 	reader 		= null;
				
				try{
					reader 					= new BufferedReader(new InputStreamReader(i));
					String 		line		= "";
					
					while((line = reader.readLine()) != null){
						
						listString = listString.concat(line);
							
					}
					
				}catch(IOException io){}
			
		 }catch(IOException e){}
		
		info.setText("You have " + (listString.split(",").length -1)+ " friends!");
		
		return listString.split(",");
	}

	private void displayFriends(String[] friends){
		
		Button[] b = new Button[friends.length];
		
		//JOSH source for bug below? due to splittings
		for(int i = 0; i < b.length -1; i++){
			
			if(!friends[i].equals("")){
				b[i] = new Button(this);
				b[i].setText(friends[i]);
				b[i].setBackgroundColor(Color.parseColor("#eeeeee"));
				b[i].setTextColor(Color.parseColor("#0000ee"));
				b[i].setPadding(0, 5, 0, 5);
				
				friendSpace.addView(b[i]);
			}
		}
		
	}
	
	
}

class MyFriendButtonListener implements OnClickListener{
	
	Button thisButton;
	
	public MyFriendButtonListener(Button b){
		this.thisButton = b;
	}

	@Override
	public void onClick(View v) {
		
		
	}

	
}





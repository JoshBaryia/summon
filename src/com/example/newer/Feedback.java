package com.example.newer;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Feedback extends Activity {
	
	private Button 				submit;
	private TextView 			info;
	private EditText 			feedbackText;
	private SharedPreferences	settings;	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_feedback);
		getActionBar().setDisplayHomeAsUpEnabled(true);

		submit 			= (Button)findViewById(R.id.feedback_submit_button);
		info 			= (TextView)findViewById(R.id.feedback_info);
		feedbackText	= (EditText)findViewById(R.id.feedback_text);
		
		
	}
	
	public 	void sendFeedback(View v){
		
		Thread trd = new Thread(new Runnable(){
			  @Override
			  public void run(){
			   send();
			  }
			});
		trd.run();
		
	}
	
	private void send(){
		
		settings 		=  getSharedPreferences("UserDetails", 0);
		String me 		= settings.getString("email", "");
		String raw 		= feedbackText.getText().toString();
		String toSend 	= raw.replace(" ", "_");
		String url 		= "http://109.255.215.99/summon/addFeedback.php";

		 try{
				HttpClient 		httpclient	= new DefaultHttpClient();
				HttpPost	 	httppost 	= new HttpPost(url);
				
				ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		        nameValuePairs.add(new BasicNameValuePair("email", me));
		        nameValuePairs.add(new BasicNameValuePair("feedback", toSend));
		        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				
				httpclient.execute(httppost);

				Toast.makeText(this, "Feedback sent, Thanks :D :D", Toast.LENGTH_SHORT).show();
				
		 }catch(Exception e){
			 	Toast.makeText(this, "Couldnt connect to db", Toast.LENGTH_SHORT).show();
		 }
		
	}

	

}

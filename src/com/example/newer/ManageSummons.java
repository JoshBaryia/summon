package com.example.newer;


import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

public class ManageSummons extends Activity implements OnClickListener{
	
	Button[] 				summonIcon;
	LinearLayout 			summonSpace;
	SharedPreferences		settings;
	
	String[] 				lat,
							lng,
							index,
							desc,
							time;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_manage_summons);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		settings 		=  getSharedPreferences("UserDetails", 0);
		summonSpace 	= (LinearLayout)findViewById(R.id.manage_summonSpace);
		
		getMySummons();
		
	}

	
	public void getMySummons(){
		
		String locations 	= "";
		
		String me 			= settings.getString("email", "");
		
		if(me.equals("")){
			
			Toast.makeText(this, "Not signed in :/...bad", Toast.LENGTH_SHORT).show();
			return;
			
		}
		
		getActionBar().setTitle("signed in as:" + me);
		
		String 			url 		= "http://109.255.215.99/summon/manageSummons.php"; 
		String 			line 		= "";
		BufferedReader 	reader;
		
		StrictMode.enableDefaults();
	
		
		 try{
				HttpClient 		httpclient	= new DefaultHttpClient();
				HttpPost	 	httppost 	= new HttpPost(url);
				
				ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
		        nameValuePairs.add(new BasicNameValuePair("email", me));
		        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		        
				HttpResponse 	response 	= httpclient.execute(httppost);
				HttpEntity 		entity		= response.getEntity();
				InputStream 	i 			= entity.getContent();
				reader 						= null;
				reader 						= new BufferedReader(new InputStreamReader(i));
			
				while((line = reader.readLine()) != null){
				
					locations = locations.concat(line);
					
				}
				
		 }catch(Exception e){
			 Toast.makeText(this, "Couldnt connect to the server", Toast.LENGTH_SHORT).show();
			 return;
		 }
		 
		
		 
		String[] str = locations.split("�");
		
		
		if(str.length < 2){
			Toast.makeText(this, "you dont have any summons! make some :) !", Toast.LENGTH_SHORT).show();
			return;
		}
		
		lat 		= new String[str.length];
		lng			= new String[str.length];
		index		= new String[str.length];
		desc		= new String[str.length];
		time		= new String[str.length];
		summonIcon 	= new Button[str.length];
		
		for(int i = 0; i < str.length  ; i++){
			
			lat[i] 			= str[i].split("~")[0].trim();
			lng[i] 			= str[i].split("~")[1].trim();
			index[i] 		= str[i].split("~")[2].trim();
			desc[i] 		= str[i].split("~")[3].trim().replace("_", " ");
			time[i]     	= str[i].split("~")[4].trim();
			
			summonIcon[i] 	= new SummonButton(this, Integer.parseInt(index[i]));
			summonIcon[i].setText(desc[i]);
			summonIcon[i].setOnClickListener(this);
			
			summonSpace.addView(summonIcon[i]);
			
			
		}
		
	}

	public void deleteSummon(final int id){
		
		//StrictMode.enableDefaults();

		//summonSpace.removeView(summonIcon[id]);
		
		for(int i = 0; i < summonIcon.length; i++){
			if(summonIcon[i].getId() == id){
				summonSpace.removeView(summonIcon[i]);
			}
		}
		
		Thread t = new Thread(new Runnable(){
			@Override
			public void run() {
				deleteSummonFromDb(id);
			}
		});
		t.start();
		
	}
	
	public void deleteSummonFromDb(int id){
		
		String url = "http://109.255.215.99/summon/deleteSummon.php";
		
		try{
			
			HttpClient 	httpclient	= new DefaultHttpClient();
			
			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
			nameValuePairs.add(new BasicNameValuePair("id",Integer.toString(id)));
			HttpPost 	httppost 	= new HttpPost(url);
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			
			httpclient.execute(httppost);
			
			}catch(Exception e){
				Toast.makeText(this, "Exception :/", Toast.LENGTH_SHORT).show();
			}
		
		
	}


	@Override
	public void onClick(View v) {
		
		SummonButton b = (SummonButton)v;
		
		deleteSummon(
					b.id
						);
		
		
	}
	
	
}

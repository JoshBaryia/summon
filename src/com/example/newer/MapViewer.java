package com.example.newer;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.location.Location;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.FragmentActivity;
import android.text.format.Time;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


/**
 * get new locations every ten seconds
 * 
 * all summons appear properly
 * 
 * */


@SuppressLint("NewApi")
public class MapViewer extends FragmentActivity implements
							GooglePlayServicesClient.ConnectionCallbacks,
							GooglePlayServicesClient.OnConnectionFailedListener,
							LocationListener{

	private static final String PREFS_NAME = "UserDetails";
	private GoogleMap map;
	
	
	
	private Button 		createButton,
						refreshButton,
						friendsButton,
						logoutButton;
	
	// New stuff below.
	LocationClient		locClient;
	Location			myLoc;
	LocationRequest		locationRequest;
	
		
	private SharedPreferences	settings;	
	private TextView 			info;
	
	
	@SuppressLint("NewApi")
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map_viewer);
		
		settings 		=  getSharedPreferences(PREFS_NAME, 0);
		createButton 	= (Button)findViewById(R.id.map_viewer_create);
		map 			= ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapFrag)).getMap();
		info 			= (TextView)findViewById(R.id.map_viewer_info);
		refreshButton	= (Button)findViewById(R.id.map_viewer_refresh);
		refreshButton.setClickable(false); // see onConnected for why
		
		locClient		= new LocationClient(this,this,this);
	//	locClient.connect();
		
		map.setMyLocationEnabled(true);
		
		
		addSummons();
		getMySummons();
		
	}
	
	@Override
	protected void onStart(){
		super.onStart();
		locClient.connect();
	}
	
	@Override
	protected void onStop(){
		super.onStop();
		locClient.disconnect();
	}
	
	/**
	private LatLng[] getSummons() {
		
		String locations 	= "";
		String url 			= "http://109.255.215.99/summon/getSummons.php";
								
		getActionBar().setTitle("Signed in as: " +settings.getString("email",""));

		info.setText(url);
		
		/**
		 * SELECT* FROM summon 
		 * INNER JOIN friend 
		 * ON friend.email2 = summon.creator
		 * WHERE friend.email1 = "A";
		 * 
		 * (1)
		 * SELECT* FROM summon WHERE creator IN(
		 * 
		 * 		SELECT email1
		 * 		FROM friend 
		 * 		WHERE email2="A"
		 * 
		 * );
		 * 
		 * (2)
		 * 
		 * SELECT* FROM  summon WHERE creator IN(
		 * 
		 * 		SELECT email2
		 * 		FROM friend
		 * 		WHERE email1="A" 
		 * );
		 * 
		 * *
		
		
		String 			line = "";
		BufferedReader 	reader;
		
		StrictMode.enableDefaults();
	
		
		 try{
				HttpClient 		httpclient	= new DefaultHttpClient();
				HttpPost	 	httppost 	= new HttpPost(url);
				
				ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
		        nameValuePairs.add(new BasicNameValuePair("email", settings.getString("email", "")));
		        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		        
				HttpResponse 	response 	= httpclient.execute(httppost);
				HttpEntity 		entity		= response.getEntity();
				InputStream 	i 			= entity.getContent();
				reader 						= null;
				reader 						= new BufferedReader(new InputStreamReader(i));
			
				while((line = reader.readLine()) != null){
				
					locations = locations.concat(line);
					
				}
				
				
		 }catch(Exception e){
			 
			 Toast.makeText(this, "map exception of some sort", Toast.LENGTH_SHORT).show();
			 
		 }
		 
		
		LatLng[] xy = getLatLng(locations.split(":"));

		return xy;
	}	
	
	*/
	
	/**
	 *  takes in csv's for lat and lng and returns an array of latlng objects
	 *
	private LatLng[] getLatLng(String[] locationString){
		
		LatLng[] cds = new LatLng[locationString.length];
		String[] tmp = new String[2];

		for(int i = 0; i < cds.length-1; i ++){
				tmp = locationString[i].trim().split(",");
				
				cds[i] = new LatLng( Double.parseDouble(tmp[0]),
								Double.parseDouble(tmp[1]));
				
				map.addMarker(new MarkerOptions()
									.position(new LatLng(cds[i].latitude, cds[i].longitude))
									.title(cds[i].latitude+ " , " + cds[i].longitude));
			
		}
		
		return cds;
		
	}*/
	
	
	/**
	 * add summons to the map.
	 *
	private void addSummons(){
		
		LatLng[] locations = getSummons();
		
		/// last in array of locatoins is probably null cos of the string splitting.
		for(int i = 0; i < locations.length -1; i++){
			
				map.addMarker(new MarkerOptions().position(locations[i])
												.title(Integer.toString(i))	);
				
		}
		
	}*/
	
	private LatLng getLatLng(){
		
		myLoc 		= locClient.getLastLocation();
		
		return new LatLng(
				locClient.getLastLocation().getLatitude(),
				locClient.getLastLocation().getLongitude()
				);

	}

	/*
	private LatLng getLatLngClient(){
	///	context callback connection failed
		 
	//	LatLng mLoc = mClient.getLastLocation();


		LatLng myL = new LatLng(	
						mClient.getLastLocation().getLatitude(),
						mClient.getLastLocation().getLongitude()
						);
		
		Toast.makeText(this, "Client" + myL.latitude + " , " + myL.longitude, Toast.LENGTH_SHORT).show();
		
		return myL;
		
	}*/
	
	/**
	 * Get the summons i've made and put them on the map.
	 * *
	private void getMySummons(){
		
		String 				locations 	= "";
		String 				url 		= "http://109.255.215.99/summon/getMySummons.php";
		String 				line 		= "";
		BufferedReader 		reader;
		
		StrictMode.enableDefaults();
	
		
		 try{
				HttpClient 		httpclient	= new DefaultHttpClient();
				HttpPost	 	httppost 	= new HttpPost(url);
				HttpResponse 	response 	= httpclient.execute(httppost);
				HttpEntity 		entity		= response.getEntity();
				InputStream 	i 			= entity.getContent();
				reader 						= null;
				reader 						= new BufferedReader(new InputStreamReader(i));
			
				while((line = reader.readLine()) != null){
				
					locations = locations.concat(line);
				
				}
				
		 }catch(Exception e){
			 Toast.makeText(this, "Couldn't get your summons?!", Toast.LENGTH_SHORT).show();
		 }
		 
		
		LatLng[] locs = getLatLng(locations.split(":"));

		
		for(int i = 0; i < locs.length - 1; i++){
			
			map.addMarker(new MarkerOptions()
								.position(locs[i])
								.title(locs[i].latitude + " , " + locs[i].longitude)
								.icon(BitmapDescriptorFactory.defaultMarker(
												BitmapDescriptorFactory.HUE_GREEN
											))
								);
			
		}
		
	}*/
	
	private void getMySummons(){
		
		String 				url 		= "http://109.255.215.99/summon/getMySummonsTest.php";// ?email=" + me;
		String 				line 		= "";
		BufferedReader 		reader;
		String 				locations 	= "";
		String 				me 			= settings.getString("email", "");
		String[] 			lat,
							lng,
							creators,
							desc;
		
		if(me.equals("")){
			Toast.makeText(this, "Something went wrong, You're not signed in...", Toast.LENGTH_SHORT).show();
			return;
		}
		
		getActionBar().setTitle("signed in as:" + me);
		
		StrictMode.enableDefaults();
	
		
		 try{
				HttpClient 		httpclient	= new DefaultHttpClient();
				HttpPost	 	httppost 	= new HttpPost(url);
				
				ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		        nameValuePairs.add(new BasicNameValuePair("email", me));
		        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			        
				HttpResponse 	response 	= httpclient.execute(httppost);
				HttpEntity 		entity		= response.getEntity();
				InputStream 	i 			= entity.getContent();
				reader 						= null;
				reader 						= new BufferedReader(new InputStreamReader(i));
			
				while((line = reader.readLine()) != null){
				
					locations = locations.concat(line);
					
				}
				
				Log.d("Josh", locations);
				
		 }catch(Exception e){
			 Toast.makeText(this, "Couldnt connect to web (Test)", Toast.LENGTH_SHORT).show();
			 return;
		 }
		 
		String[] str = locations.split(":");
		
		if(str.length < 2){
			Toast.makeText(this, "No summons!", Toast.LENGTH_SHORT).show();
			return;
		}
		
		lat 		= new String[str.length];
		lng			= new String[str.length];
		creators	= new String[str.length];
		desc		= new String[str.length];
		
		
		for(int i = 0; i < str.length  ; i++){
			
			lat[i] 		= str[i].split("~")[0].trim();
			lng[i] 		= str[i].split("~")[1].trim();
			creators[i] = str[i].split("~")[2].trim();
			desc[i] 	= str[i].split("~")[3].trim().replace("_", " ");

		}
		
		
		LatLng[] locs = new LatLng[lat.length];
		
		for(int i = 0; i < lat.length ; i++){
			
			locs[i] = new LatLng(
						Double.parseDouble(lat[i]),
						Double.parseDouble(lng[i])
					);
			
			map.addMarker(new MarkerOptions()
							.position(locs[i])
							.title("Me")
							.snippet(desc[i])
							.icon(BitmapDescriptorFactory.defaultMarker(
												BitmapDescriptorFactory.HUE_GREEN
											))
					);
			
		}
		
	}

	private void addSummons(){
		
		
		String 			locations 		= "";
		String 			url 			= "http://109.255.215.99/summon/getSummonsTest.php"; 
		String 			me 				= settings.getString("email", "");
		String			line 			= "";
		BufferedReader 	reader;
		
		String[] 		lat, 
						lng, 
						creators,
						desc;
		
		Time[] 			tOC,
						duration;
		
		if(me.equals("")){
			Toast.makeText(this, "Something went wrong, Please try to sign in again.", Toast.LENGTH_SHORT).show();
			return;
		}
		
		getActionBar().setTitle("signed in as:" + me);
		StrictMode.enableDefaults();
	
		
		 try{
				HttpClient 		httpclient	= new DefaultHttpClient();
				HttpPost	 	httppost 	= new HttpPost(url);
				
				ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
				nameValuePairs.add(new BasicNameValuePair("email", me));
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				
				HttpResponse 	response 	= httpclient.execute(httppost);
				HttpEntity 		entity		= response.getEntity();
				InputStream 	i 			= entity.getContent();
				reader 						= null;
				reader 						= new BufferedReader(new InputStreamReader(i));
			
				while((line = reader.readLine()) != null){
				
					locations = locations.concat(line);
					
				}
				
		 }catch(Exception e){
			 Toast.makeText(this, "Couldnt connect to web (Test)", Toast.LENGTH_SHORT).show();
			 return;
		 }
		  
		String[] str = locations.split("�");
		
		if(str.length < 2){
			Toast.makeText(this, "No summons!", Toast.LENGTH_SHORT).show();
			return;
		}
		
		lat 		= new String[str.length];
		lng			= new String[str.length];
		creators	= new String[str.length];
		desc		= new String[str.length];
		tOC 		= new Time[str.length];
		duration 	= new Time[str.length];
		
		
		for(int i = 0; i < str.length  ; i++){
			
			lat[i] 			= str[i].split("~")[0].trim();
			lng[i] 			= str[i].split("~")[1].trim();
			creators[i] 	= str[i].split("~")[2].trim();
			desc[i] 		= str[i].split("~")[3].trim().replace("_", " "); // the replacing isnt needed anymore cos we're posting summons not get-ing them
			tOC[i] 			= new Time(str[i].split("~")[4].trim());
			duration[i]		= new Time(str[i].split("~")[5].trim());
		
		}
		
		LatLng[] locs = new LatLng[lat.length];
		
		for(int i = 0; i < lat.length ; i++){
			
			
			locs[i] = new LatLng(
						Double.parseDouble(lat[i]),
						Double.parseDouble(lng[i])
					);
			
			map.addMarker(new MarkerOptions()
							.position(locs[i])
							.title(creators[i])
							.snippet(desc[i] + "\n over at: " + tOC[i].toString())
					);
			
		}
		info.setText("You have " + locs.length + " summons!");
		
	}


	/**
	 * 		
	 * 		OnClickers
	 * 
	 * */
	
	
	public void manageClicked(View v){
		
		Intent i = new Intent(this, ManageSummons.class);
		startActivity(i);
		
	}
	
	public void refreshClicked(View v){
		
		addSummons();
		getMySummons();
		Toast.makeText(this, "Refresh clicked " + getLatLng().latitude + " , " + getLatLng().longitude, Toast.LENGTH_SHORT).show();

	}
	
	public void createClicked(View v){

		Intent i = new Intent(this, CreateSummon.class);
		startActivity(i);
	}

	public void friendsClicked(View v){
		
		Intent i = new Intent(this, Friends.class);
		startActivity(i);

	}
	
	public void logoutClicked(View v){
		
		settings 	= getSharedPreferences(PREFS_NAME, 0);
		Editor edit = settings.edit();

		edit.putString("email", "");
		edit.commit();
		
		
		startActivity(new Intent(this, MainActivity.class));
		
	}
	
	public void feedbackClicked(View v){
		
		Intent i = new Intent(this, Feedback.class);
		startActivity(i);
		
	}
	
	/**
	 * Implemented methods
	 * */

	@Override
	public void onConnectionFailed(ConnectionResult result) {

		Toast.makeText(this, "Connection Failed try again", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onConnected(Bundle connectionHint) {
		
		Toast.makeText(this, "Connected to loc service", Toast.LENGTH_SHORT).show();
		
		map.moveCamera(
				CameraUpdateFactory.newLatLngZoom(
						getLatLng(),
						15.0f
				));
		
		
		refreshButton.setClickable(true); 		// cos if we're not connected the button will request our location and throw errors
		locationRequest		= LocationRequest.create();
		locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
		locationRequest.setInterval(20000); //20 seconds
		locClient.requestLocationUpdates(locationRequest, this);
		
	}

	@Override
	public void onDisconnected() {}

	@Override
	public void onLocationChanged(Location location) {
		
		myLoc = location;
		
	}

	
}























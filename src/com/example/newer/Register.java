package com.example.newer;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Register extends Activity {
	
	EditText	firstNameText,
				secondNameText,
				emailText,
				passwordText,
				secondPasswordText;
	TextView	instructionsText;
	
	Button		submitButton;
	
	String 		firstName,
				email,
				lastName,
				password,
				URL;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);
		getActionBar().setDisplayHomeAsUpEnabled(true);

		firstNameText		=  (EditText)findViewById(R.id.register_first_name);
		secondNameText		=  (EditText)findViewById(R.id.register_second_name);
		emailText			=  (EditText)findViewById(R.id.register_email);
		passwordText		=  (EditText)findViewById(R.id.register_password_one);
		secondPasswordText	=  (EditText)findViewById(R.id.register_password_two);
		instructionsText	=  (TextView)findViewById(R.id.register_instructions);
		
		
	}

	/**
	 * If the user presses the register button, check they entered their details properly * we shoudl prolly make a method to do this
	 * Then create execute a server side script that enteres their details into the db * after this, connect to the db and check their details exist and return a bool
	 * then redirect them to sign in * or sig them in automaticaly?
	 * */
	public void onSubmit(View v){
		
		
		if(	firstNameText		.getText().toString().equals("")
		||	secondNameText		.getText().toString().equals("")
		||	emailText			.getText().toString().equals("")
		||	passwordText		.getText().toString().equals("")
		||	secondPasswordText	.getText().toString().equals("")){
			
			Toast.makeText(this, "Please enter your details properly!", Toast.LENGTH_SHORT).show();
			
		}else{
			
			firstName 	= firstNameText		.getText().toString();
			lastName 	= secondNameText	.getText().toString();
			email 		= emailText			.getText().toString();
			password 	= passwordText		.getText().toString();
			
			//createUser();
			
			Thread t = new Thread(new Runnable(){
				@Override
				public void run(){
					createUser();
				}
			});
			t.run();
		}
		
	}
	
	/**
	 * Connects to the localhost server and executes a php file
	 * that inserts a new user into the db
	 * Returns true if nothing went wrong
	 * */
	private boolean createUser(){
		
		String url = "http://109.255.215.99/summon/addUser.php";
		
		try{
			HttpClient 		httpclient		= new DefaultHttpClient();
			HttpPost 		httppost 		= new HttpPost(url);
			
			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(4);
	        nameValuePairs.add(new BasicNameValuePair("email", email));
	        nameValuePairs.add(new BasicNameValuePair("first_name", firstName));
	        nameValuePairs.add(new BasicNameValuePair("last_name", lastName));
	        nameValuePairs.add(new BasicNameValuePair("password", password));
	        
	        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			
			httpclient.execute(httppost);
			Toast.makeText(this, "User made! :)", Toast.LENGTH_SHORT).show();
			
			return true;
			
		}catch(Exception e){
			Toast.makeText(this, "Couldnt connect!", Toast.LENGTH_SHORT).show();
			return false;
		}
		
	}

	/**
	 * Creates the url to be executed server side to create a new user.
	 * */
	private String createURL(){
		
		
		
		String url = ""
				+ "email=" 			+ email
				+ "&first_name=" 	+ firstName
				+ "&last_name=" 	+ lastName
				+ "&password=" 		+ password;
		
		instructionsText.setText(url);
		
		// setting URL the global equal to the string we just made
		URL 	= url;
		instructionsText.setText(url);
		return URL;
	}

}




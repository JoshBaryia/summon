package com.example.newer;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.text.format.Time;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;

public class CreateSummon extends Activity implements
										GooglePlayServicesClient.ConnectionCallbacks,
										GooglePlayServicesClient.OnConnectionFailedListener,
										LocationListener{

	private static final String PREFS_NAME ="UserDetails";
	
	private EditText	descriptionText,
						locationDisplay;
	private TextView	instructions;
	
	private Button		createButton,
						goBackButton;
	
	private MapViewer 	v;
	private String		useremail;
	private int 		duration;
	
	//new stuff 09/14
	LocationClient		locClient;
	Location			myLoc;
	LocationRequest		locationRequest;
	
	NumberPicker picker;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_summon);
		getActionBar().setDisplayHomeAsUpEnabled(true);

		descriptionText=(EditText)findViewById(R.id.create_description);
		locationDisplay=(EditText)findViewById(R.id.create_locationDisplay);
		locationDisplay.setText("Location will go here in a moment");
		
		instructions = (TextView)findViewById(R.id.create_instructions);
		instructions.setTextColor(Color.parseColor("#ff00ff"));
		
		createButton = (Button)findViewById(R.id.create_create);
		goBackButton = (Button)findViewById(R.id.create_goBack);
		
		createButton.setClickable(false);
		goBackButton.setClickable(false);
		
		SharedPreferences settings  = getSharedPreferences(PREFS_NAME,0);
		useremail 	= settings.getString( "email", "");

		locClient	= new LocationClient(this,this,this);
		locClient.connect();
	
		picker 		= (NumberPicker)findViewById(R.id.create_numberPicker);
		picker.setMinValue(1);
		picker.setMaxValue(60);
		picker.setWrapSelectorWheel(true);
		picker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
			
			@Override
			public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
				
				duration = picker.getValue();
			
			}
			
		});
		

		duration 	= picker.getValue();
	
	}
	
	@Override
	protected void onStart(){
		super.onStart();
		locClient.connect();
	}
	
	@Override 
	protected void onStop(){
		super.onStop();
		locClient.disconnect();
	}

	
	
	private void sendRequest(){
		
		String 	url = "http://109.255.215.99/summon/createSummon.php";
		Time 	t 	= new Time();
		t.setToNow();
		
		
		
		 try{
				 HttpClient 		httpclient		=	new DefaultHttpClient();
				 HttpPost 			httppost	 	=	new HttpPost(url);
				
				 ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(6);
				 nameValuePairs.add(new BasicNameValuePair("email",useremail));
				 nameValuePairs.add(new BasicNameValuePair("duration", Integer.toString(duration)));
				 nameValuePairs.add(new BasicNameValuePair("lat", Double.toString(myLoc.getLatitude())));
				 nameValuePairs.add(new BasicNameValuePair("lng", Double.toString(myLoc.getLongitude())));
				 nameValuePairs.add(new BasicNameValuePair("description", descriptionText.getText().toString()));
				 nameValuePairs.add(new BasicNameValuePair("time", t.hour + ":" + t.minute));
				 httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				 
				 httpclient.execute(httppost);
				
			}catch(Exception e){
				
				Toast.makeText(this, "Couldn't connect to server", Toast.LENGTH_SHORT).show();
				
			}
		
		 	Toast.makeText(this, "Summon sent!", Toast.LENGTH_SHORT).show();
	}
	
	/**
	 * 
	 * OnClickers
	 * 
	 * */
	
	public void createSummon(View v){
		
		String 		details 			= descriptionText.getText().toString();
		
		if(details.equals("")) {
			Toast.makeText(this, "Please enter your details", Toast.LENGTH_SHORT).show();
		}
		
		else{
			Thread t = new Thread(new Runnable(){
				@Override
				public void run() {
					
					sendRequest();
				}
			});
			t.run();
			
		}

	}
	
	public void backClicked(View v){
		
		Location me = locClient.getLastLocation();
		Toast.makeText(this, me.getLatitude()+ " , " + me.getLongitude() , Toast.LENGTH_SHORT).show();
		
	}

	@Override
	public void onLocationChanged(Location location) {
		// change the view
		locationDisplay.setText(location.getLatitude() + "\t" + location.getLongitude());
		Log.d("JOSH", "location changed");
	}



	@Override
	public void onConnectionFailed(ConnectionResult result) {}



	@Override
	public void onConnected(Bundle connectionHint) {
		Toast.makeText(this, "Location Connected", Toast.LENGTH_SHORT).show();
		myLoc 	= locClient.getLastLocation();
		locationDisplay.setText("Connected");
		locationDisplay.setText(myLoc.getLatitude() + "\t" + myLoc.getLongitude());
		locationRequest	= LocationRequest.create();
		locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
		locationRequest.setInterval(20000);
		locationRequest.setFastestInterval(20000);
		locClient.requestLocationUpdates(locationRequest, this);
		
		createButton.setClickable(true);
		goBackButton.setClickable(true);
		
	}



	@Override
	public void onDisconnected() {}	
	
}





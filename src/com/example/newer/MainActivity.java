package com.example.newer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {
	
	// THIS WILL STORE THE USER's details....
	// this should be saved on the device..
	public String userEmail;
	
	EditText	username,
				password,
				topText;
	
	Button		register,
				signIn;

	// File name of prefs file
	private static final String PREFS_NAME = "UserDetails";
	private SharedPreferences settings;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		username 	= (EditText)findViewById(R.id.login_username);
		password 	= (EditText)findViewById(R.id.login_password);
		topText 	= (EditText)findViewById(R.id.login_logo);
		
		register 	= (Button)findViewById(R.id.login_register_button);
		signIn		= (Button)findViewById(R.id.login_sign_in);

		
		settings 	= getSharedPreferences(PREFS_NAME, 0);
		
		if(settings.getString("email", "").equals("")){
			topText.setText("not signed in");
		}else{
			topText.setText(settings.getString("email", ""));
			Intent i = new Intent(this, MapViewer.class);
			startActivity(i);
		}
			
	}
	

	
	/**
	 * User has tried to sign in
	 * */
	public void signIn(View v){
		
	
		if(username.getText().toString().equals("")
		|| password.getText().toString().equals("")){
			
			Toast.makeText(this, "Please enter your details", Toast.LENGTH_SHORT).show();
			
		}else{	// if the user exist
			
			if(validateUserDetails()){


				SharedPreferences.Editor editor = settings.edit();

				editor.putString("email", username.getText().toString());
				editor.commit();

				Intent mainPageActivity  = new Intent(this, MapViewer.class );
				startActivity(mainPageActivity);
				
				
			}else{
			
				Toast.makeText(this, "Sorry, wrong information given!", Toast.LENGTH_SHORT).show();
				
			}
			
		}
		
	}
	
	/**
	 * If user pressed this button take them to the register view
	 * */
	public void buttonClicked(View v){
		
		Intent i = new Intent(this, Register.class);
		startActivity(i);
		
	}
	
	
	
	/**
	 * Talk to the server and confirm their details exist in the sever.. if so store their details 
	 * in the phone
	 * Returns true if the user exists
	 * */
	@SuppressLint("NewApi")
	private boolean validateUserDetails(){
		
		StrictMode.enableDefaults();
		
		String email 		= username.getText().toString();
		String pwd		 	= password.getText().toString();
		String url 			= "http://109.255.215.99/summon/validateUser.php";
		String line="";
		BufferedReader reader 	= null;
		
		if(email.equals("") || pwd.equals("")){
			Toast.makeText(this, "Please enter your details!", Toast.LENGTH_SHORT).show();
			return false;
		}
		
				
		
		HttpClient 		httpclient 	= new DefaultHttpClient();
		HttpPost 		httppost 	= new HttpPost(url);

	    try {
	      
	        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
	        nameValuePairs.add(new BasicNameValuePair("email", email));
	        nameValuePairs.add(new BasicNameValuePair("password", pwd));
	        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

	        
	        HttpResponse response 	= httpclient.execute(httppost);
	        HttpEntity entity 		= response.getEntity();
	        
			reader 					= new BufferedReader(new InputStreamReader(entity.getContent()));
			
			
			while((line = reader.readLine()) != null){
				
				if(line.equals("y")){
					
					userEmail = email;
					return true;
					
				}else if(line.equals("n")){
					Toast.makeText(this, "Sorry, you entered something wrong :/", Toast.LENGTH_SHORT).show();
					return false;
				}else{
					Toast.makeText(this, "Sorry, this shouldnt have happened", Toast.LENGTH_SHORT).show();
				}
				
			}
				
	        

	    } catch (ClientProtocolException e) {
	        
	    	Toast.makeText(this, "ClientProtocol exception", Toast.LENGTH_SHORT).show();
	    	
	    } catch (IOException e) {

	    	Toast.makeText(this, "IO exception", Toast.LENGTH_SHORT).show();
	    	
	    }
	    
	    return false;
		
		
		
	}

	private boolean loggedIn(){
		
		if(settings.getString("email", "").equals("")){
			return false;
		}else{
			topText.setText(settings.getString("email", ""));
			return true;
		}


	}
	
}

package com.example.newer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Requests extends Activity {
	
	private static final String PREFS_NAME = "UserDetails";
	private SharedPreferences settings;
	
	String 			URL;

	TextView 		infoText,
					urlDisplay;
	
	String 			username;
	TextView 		requestView;
			 
	LinearLayout 	requestSpace;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_requests);
		getActionBar().setDisplayHomeAsUpEnabled(true);

		infoText 		= 	(TextView) findViewById(R.id.requests_information_text);
		
		urlDisplay 		= 	(TextView)findViewById(R.id.requests_url);
		requestSpace	=	(LinearLayout)findViewById(R.id.requests_request_space);
		
		settings		= 	getSharedPreferences(PREFS_NAME, 0);
		username 		= 	settings.getString("email", "");
		
		if(username.equals("null")){
			infoText.setText("not loggedin.. well shit");
		}else{
			infoText.setText(username);
			displayRequests(	getRequests()	);
		}
		
		
	}
	
	
	/**
	 * gets friends requests from the server and returns them as an array of strings
	 * */
	private String[] getRequests(){

		String 			url 			= "http://109.255.215.99/summon/getFriendRequests.php";
		String 			line			= "";
		String 			responseString 	= "";
		BufferedReader 	reader 			= null;
		
		if(username.equals("null")) {
			Toast.makeText(this, "No username given doofus", Toast.LENGTH_SHORT).show();
			return new String[0];
		}
		
		urlDisplay.setText(url);
		StrictMode.enableDefaults();
		
		
		 try{
				HttpClient 		httpclient	= new DefaultHttpClient();
				HttpPost 		httppost 	= new HttpPost(url);
				ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
		        nameValuePairs.add(new BasicNameValuePair("email", username));
		        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		        
				HttpResponse 	response 	= httpclient.execute(httppost);
				HttpEntity 		entity		= response.getEntity();
				InputStream 	i 			= entity.getContent();
				
				try{
					
					reader 					= new BufferedReader(new InputStreamReader(i));
					while((line = reader.readLine()) != null){
						
						responseString 		= responseString.concat(line);
						
					}
					
				}catch(IOException io){
					
					Toast.makeText(this, "IOException thrown\n e.g. couldnt connect to server", Toast.LENGTH_SHORT).show();
			}
				
		 }catch(IOException e){}
		 
			String[] requestArray =  responseString.split(",");
			return requestArray;
		
	}

	
	private void displayRequests(String[] requests){
		
		Button[] 			b 			= new Button[requests.length];
		final String 		PREFS_NAME 	= "UserDetails";
		SharedPreferences 	settings 	= getSharedPreferences(PREFS_NAME, 0);
		String 				myEmail 	= settings.getString("email", "");

		
		if((b.length -1) ==0)	urlDisplay.setText("No new requests! :(");
		
		Log.d("JOSH-REQUESTS", Integer.toString(requests.length));
				
		for(int i = 0; i < b.length -1; i++){
			
			b[i] = new Button(this);
			b[i].setText(requests[i]);
			b[i].setOnClickListener(new MyButtonListener(b[i], myEmail));
			b[i].setBackgroundColor(Color.parseColor("#eeeeee"));
			b[i].setTextColor(Color.parseColor("#000077"));
			
			requestSpace.addView(b[i]);
		}
		
	}

}

class MyButtonListener extends Activity implements OnClickListener{

	Button button;
	String me;
	
	public MyButtonListener(Button b, String email){
		this.button = b;
		this.me = email;
	}
	
	
	@Override
	public void onClick(View v) {
		confirmRequest();
		button.setClickable(false);
	}
	
	
	void confirmRequest(){
		
		// email 1 = me
		String url = "http://109.255.215.99/summon/confirmRequest.php"; 

		
		StrictMode.enableDefaults();
		
		try{
				HttpClient httpclient= new DefaultHttpClient();
				HttpPost httppost =new HttpPost(url);
				
				ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		        nameValuePairs.add(new BasicNameValuePair("email1", me));
		        nameValuePairs.add(new BasicNameValuePair("email2", button.getText().toString().trim()));
		        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		        httpclient.execute(httppost);
		        
				button.setText( "You've confirmed:" + button.getText().toString().trim());
				
		 }catch(IOException e){
			 Toast.makeText(this, "IO exception", Toast.LENGTH_SHORT).show();
		 }
		
		

		
	}
	
}

















